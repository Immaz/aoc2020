defmodule Day01 do
  @moduledoc """
  Documentation for `Day01`.
  """

  @doc """
  This function lists all combinations of `num` elements from the given `list`
  """

  def solve([head | tail], n, cum_sum, cum_list) when n > 0 do
    cl = solve(tail, n - 1, cum_sum + head, cum_list ++ [head])
    if Enum.sum(cl) == 2020 do
      cl
    else
      solve(tail, n, cum_sum, cum_list)
    end
  end

  def solve([head | tail], n, cum_sum, cum_list) do
    #    Enum.reduce(Enum.take(cum_list ++ [head], -2), fn x, acc -> acc * x end)
    cum_list
    #    |> IO.inspect()
  end

  def solve([], n, cum_sum, cum_list) do
    [-1]
  end

  @doc """
  Ex1:

  ## Examples

      iex> Day01.ex1([1721,979,366,299,675,1456])
      514579

        iex> Day01.ex1()
        793524

  """
  def ex1() do
    Shared.read_input_of_day_to_int_list("01")
    |> ex1
  end

  def ex1(input) do
    Enum.reduce(solve(input, 2, 0, []), fn x, acc -> acc * x end)
  end

  @doc """
  Ex2:
  ## Examples

        iex> Day01.ex2([1721,979,366,299,675,1456])
        241861950

        iex> Day01.ex2()
        000000

  """
  def ex2() do
    Shared.read_input_of_day_to_int_list("01")
    |> ex2
  end

  def ex2(input) do
    Enum.reduce(solve(input, 3, 0, []), fn x, acc -> acc * x end)
  end

  def main do
  end
end
